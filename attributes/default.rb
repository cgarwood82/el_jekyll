default['el_jekyll']
default['el_jekyll']['user']			= 'jekyll'
default['el_jekyll']['deploy_directory']	= '/var/www/jekyll'
default['el_jekyll']['repository']		= 'https://transmutated@bitbucket.org/transmutated/jekyllsite.git'
default['el_jekyll']['fqdn']			= 'cliffordgarwood.com'
default['el_jekyll']['alias_name']		= 'localhost'
default['el_jekyll']['web_root']		= '/var/www/jekyll/_site'
