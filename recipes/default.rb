#
# Cookbook:: el_jekyll
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.
package 'git' do
  action :install
end

package 'httpd' do
  action :install
end

include_recipe 'el_jekyll::rvm'
include_recipe 'el_jekyll::user'
include_recipe 'el_jekyll::deploy_blog'
include_recipe 'el_jekyll::webserver'

