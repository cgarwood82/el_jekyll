user node['el_jekyll']['user'] do
  manage_home true
  home "/home/#{ node['el_jekyll']['user'] }"
  password '$6$1v8yjKTToPF.dFcK$2XgOTAs5JcuL.ES31oZ5wSz3NwqhX6kYcWMu218mxoV156lWTIlH4YAW8SSvKThwz5rsCuytQGyW42ZlzrHfG1'
end
