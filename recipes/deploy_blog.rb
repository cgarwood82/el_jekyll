template '/etc/systemd/system/jekyll.service' do
  source 'jekyll.service.erb'
  owner 'root'
  group 'root'
  mode '0755'
  variables(
    :user => node['el_jekyll']['user'],
    :work_dir => node['el_jekyll']['deploy_directory']
  )
  action :create
end

directory '/var/www/jekyll' do
  owner node['el_jekyll']['user']
  group node['el_jekyll']['user'] 
  action :create
end

git node['el_jekyll']['deploy_directory'] do
  repository node['el_jekyll']['repository']
  action :sync
  user node['el_jekyll']['user']
  group node['el_jekyll']['user'] 
end

ENV['PATH'] = '/usr/local/rvm/gems/ruby-2.4.1/bin:/usr/local/rvm/gems/ruby-2.4.1@global/bin:/usr/local/rvm/rubies/ruby-2.4.1/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/rvm/bin:/root/bin'

gem_package 'jekyll'

gem_package 'bundler' do
  # Have to do an upgrade on this since Chef can't distinquish I am trying to install this
  # using the rvm provided gem. 
  action :upgrade
end

execute "bundle install" do
  command 'sudo bundle install'
  cwd node['el_jekyll']['deploy_directory']
end

service 'jekyll' do
  action [ :enable, :start ]
end
