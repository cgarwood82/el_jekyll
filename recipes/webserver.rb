template '/etc/httpd/conf.d/webapp.conf' do
  source 'webapp.conf.erb'
  variables(
    :server_name => node['el_jekyll']['fqdn'],
    :server_alias => node['el_jekyll']['alias_name'],
    :web_root => node['el_jekyll']['web_root']
  )
  owner 'root'
  group 'root'
  mode '0755'
end

service 'httpd' do
  action [ :enable, :start ]
end 
